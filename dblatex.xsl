<?xml version='1.0' encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

  <xsl:param name="latex.encoding">utf8</xsl:param>
  <xsl:param name="latex.class.options">10pt,onecolumn</xsl:param>
  <xsl:param name="xetex.font">
  <xsl:text>\usepackage{upquote}&#10;</xsl:text>
   <xsl:choose>
    <xsl:when test="contains(/article/@lang,'ja')">
	<xsl:text>\usepackage{xeCJK}&#10;</xsl:text>
	<xsl:text>\setCJKmainfont{IPAPGothic}&#10;</xsl:text>
	<xsl:text>\setCJKsansfont{IPAPMincho}&#10;</xsl:text>
	<xsl:text>\setCJKmonofont{IPAexGothic}&#10;</xsl:text>
	<xsl:text>\setmainfont{IPAPGothic}&#10;</xsl:text>
	<xsl:text>\setsansfont{IPAPMincho}&#10;</xsl:text>
	<xsl:text>\setmonofont{IPAexGothic}&#10;</xsl:text>
    </xsl:when>
    <xsl:when test="contains(/article/@lang,'ko')">
	<xsl:text>\usepackage{xeCJK}&#10;</xsl:text>
	<xsl:text>\usepackage[hangul]{kotex}&#10;</xsl:text>
	<xsl:text>\setCJKmainfont{Noto Serif CJK KR}&#10;</xsl:text>
	<xsl:text>\setCJKsansfont{Noto Sans CJK KR}&#10;</xsl:text>
	<xsl:text>\setCJKmonofont{Noto Sans Mono CJK KR}&#10;</xsl:text>
	<xsl:text>\setmainfont{Noto Serif CJK KR}&#10;</xsl:text>
	<xsl:text>\setsansfont{Noto Sans CJK KR}&#10;</xsl:text>
	<xsl:text>\setmonofont{Noto Sans Mono CJK KR}&#10;</xsl:text>
    </xsl:when>
    <xsl:when test="contains(/article/@lang,'zh')">
        <xsl:text>\usepackage{xeCJK}&#10;</xsl:text>
        <xsl:text>\setCJKmainfont{HanaMinA}&#10;</xsl:text>
	<xsl:text>\setCJKsansfont{HanaMinA}&#10;</xsl:text>
	<xsl:text>\setCJKmonofont{HanaMinA}&#10;</xsl:text>
	<xsl:text>\setmainfont{HanaMinA}&#10;</xsl:text>
	<xsl:text>\setsansfont{HanaMinA}&#10;</xsl:text>
	<xsl:text>\setmonofont{HanaMinA}&#10;</xsl:text>
    </xsl:when>
    <xsl:when test="contains(/article/@lang,'hi')">
        <xsl:text>\setmainfont{Nakula}&#10;</xsl:text>
        <xsl:text>\setsansfont{Nakula}&#10;</xsl:text>
        <xsl:text>\setmonofont{Nakula}&#10;</xsl:text>
    </xsl:when>
    <xsl:otherwise>
        <xsl:text>\setmainfont{DejaVu Serif}&#10;</xsl:text>
        <xsl:text>\setsansfont{DejaVu Sans}&#10;</xsl:text>
        <xsl:text>\setmonofont{DejaVu Sans Mono}&#10;</xsl:text>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:param>

  <xsl:param name="format">a4</xsl:param>

  <!-- no space for subtitle -->
  <xsl:template match="article/subtitle"/>

  <xsl:template match="book|article" mode="docinfo">
    <!-- Apply default settings -->
    <xsl:apply-imports/>

    <!-- pass to LaTeX -->
    <xsl:if test="$format='lt'">
      <xsl:text>\REFletterformat&#10;</xsl:text>
    </xsl:if>
    <xsl:text>\def\REFcorpauthorimage{</xsl:text>
    <xsl:value-of
	select="/article/articleinfo/corpauthor//imagedata[@format='EPS']/@fileref"/>
    <xsl:text>}&#10;</xsl:text>
    <xsl:text>\def\REFcorpauthorurl{</xsl:text>
    <xsl:value-of
	select="/article/articleinfo/corpauthor/ulink/@url"/>
    <xsl:text>}&#10;</xsl:text>
    <xsl:text>\def\REFversion{</xsl:text>
    <xsl:value-of
	select="/article/articleinfo/keywordset/keyword[@role='version']"/>
    <xsl:text>}&#10;</xsl:text>
    <xsl:text>\def\REFrevnumber{</xsl:text>
    <xsl:value-of
	select="/article/articleinfo/revhistory/revision[1]/revnumber"/>
    <xsl:text>}&#10;</xsl:text>
    <xsl:text>\def\REFdate{</xsl:text>
    <xsl:value-of
	select="/article/articleinfo/revhistory/revision[1]/date"/>
    <xsl:text>}&#10;</xsl:text>
  </xsl:template>

  <!-- xsl:template match="processing-instruction('custom-notice')" -->
  <xsl:template match="ackno">
    <!-- <xsl:if test="$ad"> -->
    <!--   <xsl:text>\begin{center} -->
    <!--   \input{</xsl:text><xsl:value-of select="$ad"/><xsl:text>} -->
    <!--   \end{center} -->
    <!--   \vfill</xsl:text> -->
    <!-- </xsl:if> -->
    <!-- visual layout - how I hate it! -->
    <xsl:text>\newpages{</xsl:text>
    <xsl:choose>
<!--        <xsl:when test="contains('pt', /article/@lang)">2</xsl:when>  -->
        <xsl:when test="contains('bg ca de es eu fr gl hu ko nl pl pt_BR ru uk',
            /article/@lang)">0</xsl:when>
        <xsl:otherwise>1</xsl:otherwise>
    </xsl:choose>
    <xsl:text>}&#10;~&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\begin{minipage}{\linewidth}&#10;</xsl:text>
    <xsl:text>{\footnotesize&#10;</xsl:text>
    <xsl:text>\textbf{&#10;\begin{center}&#10;</xsl:text>
    <xsl:call-template name="gentext">
      <xsl:with-param name="key" select="'LegalNotice'"/>
    </xsl:call-template>
    <xsl:text>\end{center}&#10;}&#10;\vspace{-2ex}&#10;</xsl:text>
    <xsl:text>\DBKlegalblock}&#10;\vspace{-2ex}&#10;</xsl:text>
    <xsl:text>{\scriptsize \DBKcopyright \\&#10;</xsl:text>
    <xsl:value-of
      select="/article/articleinfo/keywordset/keyword[@role='madeby']"/>
    <xsl:text>: \url{https://www.debian.org/doc/user-manuals#refcard}}&#10;</xsl:text>
    <xsl:text>\end{minipage}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="x-informaltable">
    <xsl:text>\begin{minipage}{\linewidth}%&#10;</xsl:text>
    <xsl:apply-imports/>
    <xsl:text>\end{minipage}&#10;</xsl:text>
  </xsl:template>

</xsl:stylesheet>
